package model.interfaces;

/**
 * 
 * Represents a flight attendant.
 */
public interface FlightAttendant extends Person {

    /**
     * 
     * @return the flight attendant's identifier
     */
    String getFlightAttendantId();

}