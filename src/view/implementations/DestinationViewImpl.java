package view.implementations;

public class DestinationViewImpl extends AddRemoveViewImpl {

	public DestinationViewImpl() {
		super();
		this.setFrameTitle("Men� Destinazioni");
		this.setLabelTitle("Airline Destinations");
		this.setLabelViewedListTitle("Destinazioni da raggiungere:");
	}
}
