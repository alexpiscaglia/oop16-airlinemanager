package view.implementations;

public class FlightAttendantViewImpl extends AddRemoveViewImpl {

	public FlightAttendantViewImpl(){
		super();
		this.setFrameTitle("Men� Assistenti di Volo");
		this.setLabelTitle("Airline Attendants");
		this.setLabelViewedListTitle("Assistenti di volo disponibili:");
	}
}
