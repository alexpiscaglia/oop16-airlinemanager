package controller.interfaces;

/**
 * Login Controller interface.
 */
public interface LoginController {

    /**
     * Verifies user credentials.
     */
    void checkUser();

}