package controller.interfaces;

/**
 * Booking Controller interface.
 */
public interface BookingController {

    /**
     * Removes a booking from the list of bookings.
     */
    void removeBooking();

}